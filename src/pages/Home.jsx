import { Swiper, Image } from "antd-mobile";
import { IonContent, IonHeader, IonPage } from "@ionic/react";
import List from "../components/List";
import styles from "./Home.css";

const logo = { src: "assets/pokemon-logo.png", text: "Pokemon" };
const Home = () => {
    const colors = ["#ace0ff", "#bcffbd", "#e4fabd", "#ffcfac"];

    const items = colors.map((color, index) => (
        <Swiper.Item key={index}>
            <div className={styles.content} style={{ background: color }}>
                <Image src={logo.src} />
            </div>
        </Swiper.Item>
    ));

    return (
        <IonPage>
            <IonHeader>
                <Swiper autoplay loop>
                    {items}
                </Swiper>
            </IonHeader>
            <IonContent fullscreen scrollEvents={true}>
                <List />
            </IonContent>
        </IonPage>
    );
};

export default Home;
