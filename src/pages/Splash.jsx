import { Image } from "antd-mobile";
import { useHistory } from "react-router-dom";
import { IonContent, IonPage } from "@ionic/react";
import { useEffect } from "react";
import "./Home.css";

const logo = { src: "assets/pokemon-logo.png", text: "Pokemon" };
const Splash = () => {
    const history = useHistory();

    useEffect(() => {
        setTimeout(() => {
            history.push("/home");
        }, 2000);
    });
    return (
        <IonPage>
            <IonContent fullscreen color="dark">
                <div className="container center">
                    <Image src={logo.src} />
                </div>
            </IonContent>
        </IonPage>
    );
};

export default Splash;
