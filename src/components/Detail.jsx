import React, { useEffect, useState } from "react";
import { Popup, Card, Image, Grid } from "antd-mobile";
import { PokeApi } from "../helper/api";

export default (props) => {
    const [poke, _poke] = useState([]);

    useEffect(() => {
        getPoke();
    }, [props.detail.show]);

    const getPoke = async () => {
        if (props.detail.show) {
            await PokeApi.get(`/pokemon/${props.detail.id}`)
                .then((resp) => {
                    if (resp.status === 200) {
                        // console.log(resp);
                        _poke(resp.data);
                    }
                })
                .catch((error) => {
                    console.log("getPokemon error, ", error);
                });
        }
    };

    return (
        <Popup
            visible={props.detail.show}
            showCloseButton
            onClose={() => {
                props._detail({ show: false, id: 0 });
            }}
        >
            {props.detail.show && poke ? (
                <>
                    <Card title={props.detail.id}>
                        <Image src={poke?.sprites?.front_default} />
                        <Card title="Stat" onClick={() => console.log(poke?.stats)}>
                            <Grid columns={2} gap={8}>
                                {poke?.stats?.map((obj, i) => {
                                    return (
                                        <Grid.Item key={i}>
                                            <div>
                                                {obj.stat.name} {obj.base_stat}
                                            </div>
                                        </Grid.Item>
                                    );
                                })}
                            </Grid>
                        </Card>
                    </Card>
                </>
            ) : null}
        </Popup>
    );
};
