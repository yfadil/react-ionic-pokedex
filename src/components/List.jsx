import React, { useEffect, useState } from "react";
import { Grid, Card, Avatar, AutoCenter } from "antd-mobile";
import { IonInfiniteScroll, IonInfiniteScrollContent, IonItem, IonList } from "@ionic/react";
import Detail from "./Detail";
import { PokeApi } from "../helper/api";

const ExploreContainer = () => {
    const [listPoke, _listPoke] = useState([]);
    const [nextPage, _nextPage] = useState(0);
    const [detail, _detail] = useState({ show: false, id: 0 });
    const [isInfiniteDisabled, setInfiniteDisabled] = useState(false);

    useEffect(() => {
        getPoke();
    }, []);

    const getPoke = async () => {
        await setInfiniteDisabled(true);
        await PokeApi.get(nextPage === 0 ? `/pokemon` : `/pokemon?offset=${nextPage}`)
            .then((resp) => {
                if (resp.status === 200) {
                    const next = resp.data.next.split("?offset=");
                    const list = resp.data.results.map((val) => {
                        return val;
                    });
                    _listPoke([...listPoke, ...list]);
                    if (resp.data.next) {
                        _nextPage(next[1]);
                        setInfiniteDisabled(false);
                    }
                }
            })
            .catch((error) => {
                console.log("getListPokemon error, ", error);
            });
    };

    return (
        <div className="container">
            <IonList>
                <Grid columns={2} gap={8}>
                    {listPoke?.map((obj, i) => {
                        const pokeIndex = obj.url.split("/")[obj.url.split("/").length - 2];
                        return (
                            <IonItem key={i}>
                                <Card key={i} onClick={() => _detail({ show: true, id: obj.name })}>
                                    <AutoCenter>
                                        <Avatar
                                            style={{ "--size": "128px" }}
                                            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokeIndex}.png`}
                                        />
                                    </AutoCenter>
                                    <AutoCenter>{obj.name}</AutoCenter>
                                </Card>
                            </IonItem>
                        );
                    })}
                </Grid>
            </IonList>

            <IonInfiniteScroll onIonInfinite={getPoke} threshold="100px" disabled={isInfiniteDisabled}>
                <IonInfiniteScrollContent
                    loadingSpinner="bubbles"
                    loadingText="Loading more data..."
                ></IonInfiniteScrollContent>
            </IonInfiniteScroll>

            <Detail detail={detail} _detail={_detail} />
        </div>
    );
};

export default ExploreContainer;
