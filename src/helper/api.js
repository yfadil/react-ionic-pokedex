import axios from "axios";

const api = {
    hubApi: {
        baseUrlApi: `https://pokeapi.co/api/v2`,
        request: async (url, options = {}) => {
            const response = await fetch(url, options);
            return response.json();
        },
    },
};

const PokeApi = axios.create({
    baseURL: api.hubApi.baseUrlApi,
    // timeout: 10000,
});

export { api, PokeApi };
